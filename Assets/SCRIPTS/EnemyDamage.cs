﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamage : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            Debug.Log("Player Damaged");
            collision.transform.GetComponent<PlayerRespawn>().PlayerDamaged();
        }

        if (collision.transform.CompareTag("Projectile"))
        {
            Debug.Log("Player Damaged");
            Destroy(collision.transform.gameObject);
            Destroy(gameObject);
        }
    }

}
