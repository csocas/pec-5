﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovPinkmand : MonoBehaviour

{


    public float runSpeed = 2;

    public float jumpSpeed = 3;

    Rigidbody2D rb2D;


    public bool betterJump = false;

    public float fallMultiplier = 0.5f;

    public float lowjumpMultiplier = 1f;

    public SpriteRenderer SpriteRenderer;

    public Animator animator;
    public GameObject projectile;
    public float vProjectile = 2f;
    bool derecha = true;
    public bool grounded = false;






    // Start is called before the first frame update
    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();

    }

    public void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(this.gameObject.transform.position, -transform.up);
        Debug.DrawLine(gameObject.transform.position, hit.point, Color.red);

        if (hit.collider != null)
        {
            print(hit.collider.gameObject);

            if (hit.distance <= 0.6f)
            {
                grounded = true;
            }
            else
            {
                grounded = false;
            }
        }
        else
        {
            grounded = false;
        }
    }


    // Update is called once per frame
    void FixedUpdate()
    {

        if (Input.GetKey("d") || Input.GetKey("right"))
        {
            rb2D.velocity = new Vector2(runSpeed, rb2D.velocity.y);
            SpriteRenderer.flipX = false;
            animator.SetBool("Run", true);
            derecha = true;
        }

        else if (Input.GetKey("a") || Input.GetKey("left"))
        {
            rb2D.velocity = new Vector2(-runSpeed, rb2D.velocity.y);
            SpriteRenderer.flipX = true;
            animator.SetBool("Run", true);
            derecha = false;

        }

        else
        {
            rb2D.velocity = new Vector2(0, rb2D.velocity.y);
            animator.SetBool("Run", false);

        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            
            if (derecha)
            {
                GameObject projectileSpawn = (GameObject)Instantiate(projectile, transform.position +new Vector3(0.5f,0f,0f), Quaternion.identity);
                projectileSpawn.GetComponent<Rigidbody2D>().AddForce(Vector3.right * vProjectile);
                Destroy(projectileSpawn, 1f);
            }
            else 
            {
                GameObject projectileSpawn = (GameObject)Instantiate(projectile, transform.position + new Vector3(-0.5f, 0f, 0f), Quaternion.identity);
                projectileSpawn.GetComponent<Rigidbody2D>().AddForce(Vector3.left * vProjectile);
                Destroy(projectileSpawn, 1f);
            }
        }

        if (Input.GetKey("space") &&grounded)
        {
            rb2D.velocity = new Vector2(rb2D.velocity.x, jumpSpeed);

        }

        if (grounded==false)
        {
            animator.SetBool("Jump", true);
            animator.SetBool("Run", false);
        }

        if (grounded==true)
        {
            animator.SetBool("Jump", false);
        }


        if (betterJump)

        {
            if (rb2D.velocity.y<0)
            {
              rb2D.velocity  += Vector2.up * Physics2D.gravity.y * (fallMultiplier) * Time.deltaTime;

            }  

            if (rb2D.velocity.y>0 && !Input.GetKey("space"))
            {
                rb2D.velocity += Vector2.up * Physics2D.gravity.y * (lowjumpMultiplier) * Time.deltaTime;

            }
        }
        
        

    

    }

}
