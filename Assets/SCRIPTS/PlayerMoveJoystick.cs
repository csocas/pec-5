﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveJoystick : MonoBehaviour
{

    private float horizontalMove = 0f;

    private float vertivalMove = 0f;

    public Joystick joystick;

    public float runSpeedHorizontal = 2;

    public float runSpeed = 1.25f;

    public float jumpSpeed = 3;

    public float doubleJumpSpeed = 2.5f;

    private bool canDoubleJump;

    Rigidbody2D rb2D;

    public SpriteRenderer SpriteRenderer;

    public Animator animator;


    public bool betterJump = false;

    public float fallMultiplier = 0.5f;

    public float lowjumpMultiplier = 1f;
    public GameObject projectile;

    public float vProjectile = 2f;
    bool derecha = true;
    public bool grounded = false;




    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();

    }

    private void Update()
    {


        if (horizontalMove > 0)
        {
            SpriteRenderer.flipX = false;
            animator.SetBool("Run", true);
        }

        else if (horizontalMove < 0)
        {
            SpriteRenderer.flipX = true;
            animator.SetBool("Run", true);
           

        }

        else
        {
            animator.SetBool("Run", false);
        }

        if (checkground.isGrounded == false)
        {
            animator.SetBool("Jump", true);
            animator.SetBool("Run", false);
        }

        if (checkground.isGrounded == true) ;
        {
            animator.SetBool("Jump", false);
            animator.SetBool("DoubleJump", false);
            animator.SetBool("Falling", false);

        }

        if (rb2D.velocity.y < 0)
        {
            animator.SetBool("Falling", true);

        }
        else if (rb2D.velocity.y > 0)
        {
            animator.SetBool("Falling", false);
        }










        RaycastHit2D hit = Physics2D.Raycast(this.gameObject.transform.position, -transform.up);
        Debug.DrawLine(gameObject.transform.position, hit.point, Color.red);

        if (hit.collider != null)
        {
            print(hit.collider.gameObject);

            if (hit.distance <= 0.6f)
            {
                grounded = true;
            }
            else
            {
                grounded = false;
            }
        }
        else
        {
            grounded = false;
        }
    }


    // Update is called once per frame
    void FixedUpdate()
    {

        horizontalMove = joystick.Horizontal * runSpeedHorizontal;
        transform.position += new Vector3(horizontalMove, 0, 0) * Time.deltaTime * runSpeed;

    }

    public void Shoot()
    {
        if (derecha)
        {
            GameObject projectileSpawn = (GameObject)Instantiate(projectile, transform.position + new Vector3(0.5f, 0f, 0f), Quaternion.identity);
            projectileSpawn.GetComponent<Rigidbody2D>().AddForce(Vector3.right * vProjectile);
            Destroy(projectileSpawn, 1f);
        }
        else
        {
            GameObject projectileSpawn = (GameObject)Instantiate(projectile, transform.position + new Vector3(-0.5f, 0f, 0f), Quaternion.identity);
            projectileSpawn.GetComponent<Rigidbody2D>().AddForce(Vector3.left * vProjectile);
            Destroy(projectileSpawn, 1f);
        }
    }




    public void Jump()
    {

        if (checkground.isGrounded)
        {
            canDoubleJump = true;
            rb2D.velocity = new Vector2(rb2D.velocity.x, jumpSpeed); 
        }
        else
        {
            if (canDoubleJump)
            {
                animator.SetBool("DoubleJump", true);
                rb2D.velocity = new Vector2(rb2D.velocity.x, doubleJumpSpeed);
                canDoubleJump = false;

            }

        }

        


    }

}