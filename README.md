PEC 3 Programación de Videojuegos 2D
Master en Diseño y Programación de Videojuegos
Universtat Oberta de Catalunya


JUEGO DE PLATAFORMAS CON DISPARO

URL Video en Youtube
https://youtu.be/H3yX8uXsN6s


URL Proyecto en GitLab
https://gitlab.com/csocas/pec-5.git


README.- 

Funcionamiento del juego

El juego básicamente funciona de manera muy similar a Super Mario. En este caso nuestro personaje también es capaz de moverse horizontalmente y verticalmente mediante saltos a las plataformas dispuestas. Puede matar a los personajes saltando encima de ellos (2 veces) en el caso de los “Angry Pigs” y en el caso de “Turtles” y cualquier enemigo también puede matarlos disparando bolas que lanza tienen una duración de 1 segundo. Por supuesto puede morir en esos enfrentamientos o con determinados objetos como pinchos, en tal caso volvería al último “checkpoint” (mástil con bandera) activado.
El escenario no se ve completamente por lo que se ha configurado un seguimiento de cámara para nuestro personaje, con sus limites y suavizado incluido para mejorar la visibilidad y no mostrar partes del escenario no adecuadas.
El juego lo componen 3 escenas, la primera es el Menú Inicial donde se muestran las instrucciones y podemos elegir (mediante puertas) si acceder al nivel 1 o ir a la pantalla de salida. Una vez que “atravesamos” la puerta de inicio del nivel aparecemos en el juego en sí mismo. Tanto en la pantalla de inicio como en el nivel 1 podemos manejar a nuestro personaje.







Estructura e implementación  

El juego está compuesto por once scripts que, de manera resumida, comprenden lo siguiente:

1.	MovPinkman: Es el script más complejo pero a pesar de contener más líneas de programación no es excesivamente difícil. Contiene los métodos y variables para poder mover a nuestro personaje y disparar y sus animaciones para la versión de pc/mac.

2.	MovPad: Este script es muy similar al anterior pero esta relacionado con un joystick situado en la esquina inferior izquierda, para moverse a izquierda y derecha; y dos botones en la esquina inferior derecha para saltar y disparar.

3.	CameraFollow: Este script es el encargado de que la cámara siga a nuestro personaje. Se ha tenido en cuenta marcarle unos límites y se ha suavizado dicho seguimiento.

4.	EnemyDamage: Para el daño que generan los enemigos.

5.	DamageObjects: Idéntico al anterior pero para los objetos.

6.	Checkpoint: Este script permita establecer el punto de checkpoint donde reaparecerá nuestro personaje si muere.

7.	PlayerRespawn: Script relacionado con el anterior para realizar la acción de reaparición después de su muerte en el punto marcado del escenario. Se ha relacionado con los tres corazones que se muestran mediante el canvas en la esquina superior izquierda, de manera que ahora muere después de colisionar con tres enemigos o/y objetos que causen daño. Con cada colisión se elimina un corazón hasta que muere al no quedarle ninguno. 

8.	FruitCollect: Este script muy sencillo es el encargado de hacer desaparecer las frutas una vez nuestro personaje colisiona con ellas.

9.	FruitManager: Este script no lo terminé de desarrollar pero básicamente se creó con la intención de llevar una puntuación mediante la recolección de frutas. Además la intención era que la puerta para superar el nivel no se activase hasta haber recolectado todas las frutas. En cualquier caso debido a la falta de tiempo (pero sobre todo de experiencia) no he sido capaz de terminarlo e implementarlo correctamente.

10.	OpenDoors: En este caso es un script en el que básicamente relaciona la colisión de nuestro personaje con las puertas para poder activar el “acceder” con la tecla “E”. En la puerta del nivel además esto ocurre a la vez que aparece el texto “¿ENTRAR?”.

11.	IABasics: Esta carpeta contiene 3 scripts (checkground,IABasics y JumpDamage) que son los encargados de la inteligencia artificial del personaje denominado AngryPig. En ellos se establece su comportamiento indicando un punto A y un punto B para su desplazamiento y otorgando tiempos de espera para ello, la coherencia entre la animación y la dirección en la que se mueve además de las necesarias líneas de programación para la forma en que podemos matarle. 

12.	Cargar menú / cargar menú2: Son scripts muy sencillitos para indicar a la pantallas de logo y titulo cuanto tiempo deben mantenerse antes de dar paso a la siguiente pantalla.

13.	ChangeDoorSkins: Este es el script específico para la puerta denominada “Opciones” del Menú Principal y que esta relacionada con el panel que nos permite cambiar la skin de nuestro personaje.

14.	PlayerSelect: Este script lo cree para cambiar de personaje fuera del juego desde el inspector de forma manual. Permite acceder a las 4 apariencias,  todas con sus respectivas animaciones de “jump”, “run” y “hit”. Como me dio tiempo para seguir desarrollando esta parte luego cree el otro script que nos permite acceder a este cambio desde el propio juego.

15.	Trampoline: Se añadió este objeto para aumentar las posibilidades en los niveles creando este sencillo script para indicar la fuerza con la que debe proyectar a nuestro personaje hacia arriba y su animación requerida.

16.	UIManager: Es el script creado para el Menú Pausa, este menú básicamente se compone de 4 botones que dan acceso al Menú Principal, a Ajustes (sin desarrollar), cerrar la aplicación o volver al juego. También contiene las líneas del botón que permanece en la esquina superior izquierda durante todo el juego y que nos da la opción de acceder a este panel del Menú Pausa.      


Conclusiones

En general estoy muy contento con el trabajo realizado, si bien es verdad que perdí mucho tiempo al principio creando un juegos de carreras de coche primero y luego otro también de plataformas pero de zombies, lo cierto es que no me arrepiento de haber seguido mejorando este videojuegos por diversas razones, como entregar un videojuego más trabajado para una entrega final de una asignatura; otra razón para seguir con este proyecto y quizás la que me hizo decantarme es que con mi escaso nivel de programación nunca había dedicado tanto tiempo a un mismo proyecto, es decir, he creado varios proyectos pequeñitos, sencillos, pero nunca había realizado un proyecto de estas características yo solo, dedicándole tanto tiempo, mejorando y ampliando scripts, animaciones y otros recursos que normalmente no vuelvo a revisar, eso me ha servido para cuestionarme formas o estructuras que pensaba en un principio adecuadas, cuidar detalles como nombrar bien los objetos, organizar el proyecto tanto en “Hierarchy” como en las carpetas y sobre todo a la hora de estructurar y comentar las diferentes programaciones, que es donde más me he perdido a la hora de buscar algo concreto trabajado tiempo atrás.

En definitiva y resumiendo, más como conclusión final de la asignatura que solo  de esta última PEC, me alegro de haber cursado esta asignatura, le he perdido el miedo a la programación a la vez que he descubierto sus infinitas posibilidades, lo que significa que hay que seguir trabajando y aprendiendo para lograr dominar o al menos ejecutar con soltura esta apasionante faceta dentro de los videojuegos.         
   

	

	
 
